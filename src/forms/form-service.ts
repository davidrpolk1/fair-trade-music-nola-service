import { Injectable } from '@nestjs/common';
import { forms } from '@googleapis/forms';
import { Observable, from, map, catchError, switchMap, combineLatest, tap, of } from 'rxjs';
import { writeFile } from 'fs/promises';
import { inspect } from 'util';
import { GoogleAuth } from 'google-auth-library';

@Injectable()
export class FormService {
  private readonly formId = '1vzkC7uVRuO_QxSUIeVEzpKMKPnFbhJKcPl4b6bC4yMY';
  auth: GoogleAuth;

  constructor() {
    this.initializeGoogleAuth();
  }

  private createGoogleApplicationCredentials() {
    const {
      GOOGLE_APPLICATION_CREDENTIALS_TYPE,
      GOOGLE_APPLICATION_CREDENTIALS_PROJECT_ID,
      GOOGLE_APPLICATION_CREDENTIALS_PRIVATE_KEY_ID,
      GOOGLE_PRIVATE_KEY,
      GOOGLE_APPLICATION_CREDENTIALS_CLIENT_EMAIL,
      GOOGLE_APPLICATION_CREDENTIALS_CLIENT_ID,
      GOOGLE_APPLICATION_CREDENTIALS_AUTH_URI,
      GOOGLE_APPLICATION_CREDENTIALS_TOKEN_URI,
      GOOGLE_APPLICATION_CREDENTIALS_AUTH_PROVIDER_X509_CERT_URL,
      GOOGLE_APPLICATION_CREDENTIALS_CLIENT_X509_CERT_URL,
      GOOGLE_APPLICATION_CREDENTIALS_UNIVERSE_DOMAIN
    } = process.env;

    const googleApplicationCredentials = {
      type: GOOGLE_APPLICATION_CREDENTIALS_TYPE,
      project_id: GOOGLE_APPLICATION_CREDENTIALS_PROJECT_ID,
      private_key_id: GOOGLE_APPLICATION_CREDENTIALS_PRIVATE_KEY_ID,
      private_key: GOOGLE_PRIVATE_KEY.replace(/\\n/g, '\n'),
      client_email: GOOGLE_APPLICATION_CREDENTIALS_CLIENT_EMAIL,
      client_id: GOOGLE_APPLICATION_CREDENTIALS_CLIENT_ID,
      auth_uri: GOOGLE_APPLICATION_CREDENTIALS_AUTH_URI,
      token_uri: GOOGLE_APPLICATION_CREDENTIALS_TOKEN_URI,
      auth_provider_x509_cert_url: GOOGLE_APPLICATION_CREDENTIALS_AUTH_PROVIDER_X509_CERT_URL,
      client_x509_cert_url: GOOGLE_APPLICATION_CREDENTIALS_CLIENT_X509_CERT_URL,
      universe_domain: GOOGLE_APPLICATION_CREDENTIALS_UNIVERSE_DOMAIN
    };

    const credsJSON = JSON.stringify(googleApplicationCredentials);
    return from(writeFile('./google-application-credentials.json', credsJSON)).pipe(
      catchError((error) => {
        console.error(`Error creating google-application-credentials.json:  ${inspect(error)}`);
        throw error;
      })
    );
  }

  private initializeGoogleAuth(): void {
    this.createGoogleApplicationCredentials()
      .pipe(
        switchMap(() => {
          const auth = new GoogleAuth({
            keyFile: './google-application-credentials.json',
            scopes: 'https://www.googleapis.com/auth/cloud-platform'
          });
          const client = from(auth.getClient());
          const projectId = from(auth.getProjectId());

          return combineLatest({ client, projectId, auth: of(auth) });
        }),
        switchMap(({ client, projectId, auth }) => {
          const url = `https://dns.googleapis.com/dns/v1/projects/${projectId}`;
          const res = from(client.request({ url }));

          return combineLatest({ res, auth: of(auth) });
        }),
        tap(({ res, auth }) => {
          console.log({ res });
          if (!res.data) throw "Couldn't get google auth";
          else this.auth = auth;
        }),
        catchError((error) => {
          console.error('Error getting Google Auth', inspect(error));
          throw error;
        })
      )
      .subscribe();
  }

  getFormData(): Observable<any> {
    const auth = this.auth;
    return from(forms('v1').forms.get({ auth, formId: this.formId })).pipe(
      map((form) => {
        console.log({ form });
        return form;
      }),
      catchError((error) => {
        console.error(inspect(error));
        throw error;
      })
    );
  }
}
