import { Controller, Get } from '@nestjs/common';
import { Observable, catchError } from 'rxjs';
import { FormService } from './form-service';
import { inspect } from 'util';

@Controller('/form')
export class FormController {
  constructor(private readonly formService: FormService) {}

  @Get()
  getFormData(): Observable<any> {
    return this.formService.getFormData().pipe(
      catchError((error) => {
        console.error(inspect(error));
        throw error;
      })
    );
  }
}
