import 'dotenv/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
// import { GoogleAuth } from 'google-auth-library';
// import { writeFile } from 'node:fs/promises';
// import { inspect } from 'node:util';

// async function createGoogleApplicationCredentials(): Promise<void> {
//   const {
//     GOOGLE_APPLICATION_CREDENTIALS_TYPE,
//     GOOGLE_APPLICATION_CREDENTIALS_PROJECT_ID,
//     GOOGLE_APPLICATION_CREDENTIALS_PRIVATE_KEY_ID,
//     GOOGLE_PRIVATE_KEY,
//     GOOGLE_APPLICATION_CREDENTIALS_CLIENT_EMAIL,
//     GOOGLE_APPLICATION_CREDENTIALS_CLIENT_ID,
//     GOOGLE_APPLICATION_CREDENTIALS_AUTH_URI,
//     GOOGLE_APPLICATION_CREDENTIALS_TOKEN_URI,
//     GOOGLE_APPLICATION_CREDENTIALS_AUTH_PROVIDER_X509_CERT_URL,
//     GOOGLE_APPLICATION_CREDENTIALS_CLIENT_X509_CERT_URL,
//     GOOGLE_APPLICATION_CREDENTIALS_UNIVERSE_DOMAIN
//   } = process.env;

//   const googleApplicationCredentials = {
//     type: GOOGLE_APPLICATION_CREDENTIALS_TYPE,
//     project_id: GOOGLE_APPLICATION_CREDENTIALS_PROJECT_ID,
//     private_key_id: GOOGLE_APPLICATION_CREDENTIALS_PRIVATE_KEY_ID,
//     private_key: GOOGLE_PRIVATE_KEY.replace(/\\n/g, '\n'),
//     client_email: GOOGLE_APPLICATION_CREDENTIALS_CLIENT_EMAIL,
//     client_id: GOOGLE_APPLICATION_CREDENTIALS_CLIENT_ID,
//     auth_uri: GOOGLE_APPLICATION_CREDENTIALS_AUTH_URI,
//     token_uri: GOOGLE_APPLICATION_CREDENTIALS_TOKEN_URI,
//     auth_provider_x509_cert_url: GOOGLE_APPLICATION_CREDENTIALS_AUTH_PROVIDER_X509_CERT_URL,
//     client_x509_cert_url: GOOGLE_APPLICATION_CREDENTIALS_CLIENT_X509_CERT_URL,
//     universe_domain: GOOGLE_APPLICATION_CREDENTIALS_UNIVERSE_DOMAIN,
//   };

//   try {
//     const credsJSON = JSON.stringify(googleApplicationCredentials);
//     await writeFile('./google-application-credentials.json', credsJSON);
//   } catch (error) {
//     console.error(`Error creating google-application-credentials.json:  ${inspect(error)}`);
//   }
// }

// async function initializeGoogleAuth() {
//   try {
//     const auth = new GoogleAuth({
//       keyFile: './google-application-credentials.json',
//       scopes: 'https://www.googleapis.com/auth/cloud-platform',
//     });
//     const client = await auth.getClient();
//     const projectId = await auth.getProjectId();
//     const url = `https://dns.googleapis.com/dns/v1/projects/${projectId}`;
//     const res = await client.request({ url });
//     console.log({ res });
//     if (!res.data) throw "Couldn't get google auth";
//   } catch (error) {
//     console.error(`Error creating Google Auth: ${inspect(error)}`);
//   }
// }

async function bootstrap() {
  // await createGoogleApplicationCredentials();
  // await initializeGoogleAuth();
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(3000);
  console.log('App listening on port 3000');
}
bootstrap();
